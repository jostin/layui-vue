::: demo

<template>
  <lay-field title="标题">内容</lay-field>
</template>

<script>
import { ref } from 'vue'

export default {
  setup() {

    return {
    }
  }
}
</script>

:::

::: demo

<template>
  <lay-field title="标题">内容</lay-field>
</template>

<script>
import { ref } from 'vue'

export default {
  setup() {

    return {
    }
  }
}
</script>

:::

::: demo

<template>
  <lay-field title="标题"></lay-field>
</template>

<script>
import { ref } from 'vue'

export default {
  setup() {

    return {
    }
  }
}
</script>

:::

::: title 字段属性
:::

::: table

| Name  | Description | Accepted Values |
| ----- | ----------- | --------------- |
| title | 标题        | --              |

:::
